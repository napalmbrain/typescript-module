import { URL, pathToFileURL } from 'url';
import { cwd } from 'process';
import { transformSync } from 'esbuild';
import { isTypeScriptSpecifier, hasTypeScriptExtension } from '../lib/utils.mjs';

const baseURL = pathToFileURL(`${cwd()}/`).href;

export function resolve(specifier: string, context: any, defaultResolve: any) {
  const { parentURL = baseURL } = context;
  if(isTypeScriptSpecifier(specifier) || hasTypeScriptExtension(specifier)) {
    return {
      url: new URL(`${specifier}${hasTypeScriptExtension(specifier) ? "" : ".ts"}`, parentURL).href
    };
  }
  return defaultResolve(specifier, context, defaultResolve);
}

export function getFormat(url: string, context: any, defaultGetFormat: any) {
  if(hasTypeScriptExtension(url)) {
    return {
      format: 'module'
    };
  }
  return defaultGetFormat(url, context, defaultGetFormat);
}

export function transformSource(source: Buffer, context: any, defaultTransformSource: any) {
  const { url } = context;
  if(hasTypeScriptExtension(url)) {
    return {
      source: transformSync(source.toString(), {
        loader: 'ts',
        format: 'esm'
      }).code
    };
  }
  return defaultTransformSource(source, context, defaultTransformSource);
}