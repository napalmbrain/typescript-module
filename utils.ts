import { copyFile, mkdir, stat } from 'fs/promises';
import { normalize } from 'path/posix';
import { builtinModules } from 'module';
import { globby } from 'globby';

const typescript = /\.ts$/;
const extensions = /\.[^\/]+$/;

export let nodeModules = await globby(['node_modules/**'], {
  onlyDirectories: true,
  deep: 1
});

nodeModules = nodeModules.map((module) => {
  return module.replace('node_modules/', '');
});

function isBuiltIn(s: string) {
  const found = builtinModules.findIndex((module) => {
    return s.endsWith(module);
  });
  return found >= 0;
}

function isNodeModule(s: string) {
  const found = nodeModules.findIndex((module) => {
    return s.startsWith(module);
  });
  return found >= 0;
}

function hasFileExtension(s: string) {
  return extensions.test(s);
}

export function hasTypeScriptExtension(s: string) {
  return typescript.test(s);
}

export function isTypeScriptSpecifier(s: string) {
  return !(isBuiltIn(s) || isNodeModule(s) || hasFileExtension(s));
}

export async function copy(path: string, dest: string) {
  path = normalize(path);
  const stats = await stat(path);
  if(stats.isDirectory()) {
    await mkdir(`${dest}/${path}`, { recursive: true });
    const contents = await globby(`${path}/**/*`);
    for(const content of contents) {
      await copy(content, dest);
    }
  } else {
    await copyFile(path, `${dest}/${path}`);
  }
}