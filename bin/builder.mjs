#!/usr/bin/env node
import { exit } from "process";
import { build } from "esbuild";
import { builtinModules } from "module";
import { copy, nodeModules } from "../lib/utils.mjs";
import yargs from "yargs";
const parser = yargs(process.argv.slice(2)).scriptName("typescript-module").usage("$0 [files...]", "build a script", function(yargs2) {
  yargs2.positional("files", {
    describe: "The entrypoints.",
    type: "array"
  });
}).options({
  "outdir": {
    default: "./build",
    describe: "Output directory.",
    type: "string"
  },
  "assets": {
    default: [],
    describe: "Included assets.",
    type: "array"
  },
  "sourcemap": {
    default: false,
    describe: "Add source maps.",
    type: "boolean"
  }
});
if (!parser.argv.files) {
  parser.showHelp();
  exit(1);
}
const argv = parser.argv;
if (argv.assets.length) {
  for (const asset of argv.assets) {
    await copy(asset, argv.outdir);
  }
}
const options = {
  external: builtinModules.concat(nodeModules).map((module) => `${module}*`),
  color: true,
  entryPoints: argv.files,
  outdir: argv.outdir,
  sourcemap: argv.sourcemap,
  outExtension: { ".js": ".mjs" },
  bundle: true,
  format: "esm"
};
await build(options);
