import { URL, pathToFileURL } from "url";
import { cwd } from "process";
import { transformSync } from "esbuild";
import { isTypeScriptSpecifier, hasTypeScriptExtension } from "../lib/utils.mjs";
const baseURL = pathToFileURL(`${cwd()}/`).href;
function resolve(specifier, context, defaultResolve) {
  const { parentURL = baseURL } = context;
  if (isTypeScriptSpecifier(specifier) || hasTypeScriptExtension(specifier)) {
    return {
      url: new URL(`${specifier}${hasTypeScriptExtension(specifier) ? "" : ".ts"}`, parentURL).href
    };
  }
  return defaultResolve(specifier, context, defaultResolve);
}
function getFormat(url, context, defaultGetFormat) {
  if (hasTypeScriptExtension(url)) {
    return {
      format: "module"
    };
  }
  return defaultGetFormat(url, context, defaultGetFormat);
}
function transformSource(source, context, defaultTransformSource) {
  const { url } = context;
  if (hasTypeScriptExtension(url)) {
    return {
      source: transformSync(source.toString(), {
        loader: "ts",
        format: "esm"
      }).code
    };
  }
  return defaultTransformSource(source, context, defaultTransformSource);
}
export {
  getFormat,
  resolve,
  transformSource
};
