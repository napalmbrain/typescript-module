#!/usr/bin/env node --experimental-vm-modules --loader ./loader.mjs

import vm from 'vm';
import { build } from 'esbuild';
import { builtinModules } from 'module';
import yargs from 'yargs';

const argv = await yargs(process.argv.slice(2))
  .usage('$0 <script>', 'run a script', function(yargs: any) {
    yargs.positional('script', {
      describe: 'The script to run.',
      type: 'string'
    })
  })
  .argv;

const context: vm.Context = vm.createContext({
  ...global,
  atob,
  btoa,
  console,
  performance,
  process,
  AbortController,
  AbortSignal,
  Buffer,
  Event,
  EventTarget,
  MessageChannel,
  MessageEvent,
  MessagePort,
  TextDecoder,
  TextEncoder,
  URL,
  URLSearchParams,
  WebAssembly
});

interface IModuleCache {
  has(key: string): boolean
  get(key: string): vm.Module
  set(key: string, module: vm.Module): void
}

class ModuleCache implements IModuleCache {
  store: { [key: string]: any };
  constructor() {
    this.store = { };
  }
  has(key: string): boolean {
    return this.store.hasOwnProperty(key);
  }
  get(key: string): vm.Module {
    if(!this.has(key)) {
      throw new Error(`${key} not in module cache.`);
    }
    return this.store[key];
  }
  set(key: string, module: vm.Module) {
    if(this.has(key)) {
      throw new Error(`${key} already in module cache.`);
    }
    this.store[key] = module;
  }
}

const cache: ModuleCache = new ModuleCache();

async function importModule(specifier: string, referencingModule: vm.Module) {
  if(cache.has(specifier)) {
    return cache.get(specifier);
  }
  const module = await import(specifier);
  const exports = Object.keys(module);
  const synthetic = new vm.SyntheticModule(exports, function() {
    for(const name of exports) {
      this.setExport(name, module[name]);
    }
  }, {
    identifier: specifier,
    context: referencingModule.context
  });
  cache.set(specifier, synthetic);
  return synthetic;
}

async function link(specifier: string, referencingModule: vm.Module) {
  return await importModule(specifier, referencingModule);
}

await importModule(argv.script, context);

