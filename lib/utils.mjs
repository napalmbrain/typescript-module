import { copyFile, mkdir, stat } from "fs/promises";
import { normalize } from "path/posix";
import { builtinModules } from "module";
import { globby } from "globby";
const typescript = /\.ts$/;
const extensions = /\.[^\/]+$/;
let nodeModules = await globby(["node_modules/**"], {
  onlyDirectories: true,
  deep: 1
});
nodeModules = nodeModules.map((module) => {
  return module.replace("node_modules/", "");
});
function isBuiltIn(s) {
  const found = builtinModules.findIndex((module) => {
    return s.endsWith(module);
  });
  return found >= 0;
}
function isNodeModule(s) {
  const found = nodeModules.findIndex((module) => {
    return s.startsWith(module);
  });
  return found >= 0;
}
function hasFileExtension(s) {
  return extensions.test(s);
}
function hasTypeScriptExtension(s) {
  return typescript.test(s);
}
function isTypeScriptSpecifier(s) {
  return !(isBuiltIn(s) || isNodeModule(s) || hasFileExtension(s));
}
async function copy(path, dest) {
  path = normalize(path);
  const stats = await stat(path);
  if (stats.isDirectory()) {
    await mkdir(`${dest}/${path}`, { recursive: true });
    const contents = await globby(`${path}/**/*`);
    for (const content of contents) {
      await copy(content, dest);
    }
  } else {
    await copyFile(path, `${dest}/${path}`);
  }
}
export {
  copy,
  hasTypeScriptExtension,
  isTypeScriptSpecifier,
  nodeModules
};
